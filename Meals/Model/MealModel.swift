//
//  MealModel.swift
//  Meals
//
//  Created by Woessner, Philipp on 26.11.19.
//  Copyright © 2019 sap. All rights reserved.
//

import Foundation
import UIKit

struct MealModel {
    var name: String
    var image: UIImage?
    var rating: Int
    
    public static func createDummyMeals() -> [MealModel] {
        let meal1 = MealModel(name: "Spaghetti", image: UIImage(named: "Pasta"), rating: 2)
        let meal2 = MealModel(name: "Pizza", image: UIImage(named: "Pizza"), rating: 4)
        let meal3 = MealModel(name: "Burger", image: UIImage(named: "Burger"), rating: 1)
        let meal4 = MealModel(name: "Fries", image: UIImage(named: "Fries"), rating: 5)
        return [meal1, meal2, meal3, meal4]
    }
}
