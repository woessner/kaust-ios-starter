//
//  APICall.swift
//  Meals
//
//  Created by Woessner, Philipp on 27.11.19.
//  Copyright © 2019 sap. All rights reserved.
//

import Foundation

struct APOD: Codable {
    var copyright: String
    var date: String
    var explanation: String
    var hdurl: String
    var media_type: String
    var service_version: String
    var title: String
    var url: String
}

struct PostData: Encodable {
    var meal: String
    var customer: String
}

class APICall {
    
    /* How you call the API
     
     APICall.getNasaCall { result in
                do {
                    let apiData = try result.get()
                    print(apiData)
                } catch let error {
                    print(error)
                }
            }
     */
    
    static func getNasaCall(completionHandler: @escaping (_: Result<APOD,Error>) -> Void) {
        let nasaUrl = URL(string: "https://api.nasa.gov/planetary/apod?api_key=9SB5X2tPEgdaUf1m5RtHeRN2loHUr33rsaCXABlV")
        var request = URLRequest(url: nasaUrl!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(.failure(error!))
                return
            }
            
            do {
                let apodResult = try JSONDecoder().decode(APOD.self, from: data!)
                completionHandler(.success(apodResult))
            } catch let error {
                completionHandler(.failure(error))
            }
        }
        
        task.resume()
    }
    
    
    // Example request on how you would perform a POST request
    static func postNasaCall(completionHandler: @escaping (_: Result<APOD,Error>) -> Void) throws {
        let nasaUrl = URL(string: "https://your-url-here.com")
        var request = URLRequest(url: nasaUrl!)
        request.httpMethod = "POST"
        request.httpBody = try JSONEncoder().encode(PostData(meal: "Pizza", customer: "Paul"))
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard error == nil else {
                completionHandler(.failure(error!))
                return
            }
            
            do {
                let apodResult = try JSONDecoder().decode(APOD.self, from: data!)
                completionHandler(.success(apodResult))
            } catch let error {
                completionHandler(.failure(error))
            }
        }
        
        task.resume()
    }
}
