//
//  RatingControl.swift
//  Meals
//
//  Created by Woessner, Philipp on 26.11.19.
//  Copyright © 2019 sap. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIStackView {
    
    var rating = 2 {
        didSet {
            setupButtons()
        }
    }
    
    private var buttonStack = [UIButton]()
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupButtons()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
        setupButtons()
    }
    
    
    private func setupButtons() {
        
        clearButtons()
        
        for index in 0..<5 {
            let button = UIButton()
            buttonStack.append(button)
            
            let image = index < rating ? UIImage(named: "On") : UIImage(named: "Off")
            button.setImage(image, for: .normal)
            
            button.addTarget(self, action: #selector(submitRating), for: .touchUpInside)
            
            button.translatesAutoresizingMaskIntoConstraints = false
            button.widthAnchor.constraint(equalToConstant: 40).isActive = true
            button.heightAnchor.constraint(equalToConstant: 40).isActive = true
            
            addArrangedSubview(button)
        }
    }
    
    private func clearButtons() {
        for button in buttonStack {
            self.removeArrangedSubview(button)
        }
        buttonStack.removeAll()
    }
    
    @objc func submitRating (_ sender: UIButton) {
        let index = buttonStack.firstIndex(of: sender)
        rating = index ?? 0
        rating += 1
    }
    
}
