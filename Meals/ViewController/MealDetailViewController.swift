//
//  ViewController.swift
//  Meals
//
//  Created by Woessner, Philipp on 25.11.19.
//  Copyright © 2019 sap. All rights reserved.
//

import UIKit

class MealDetailViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    var meal: MealModel?
    
    @IBOutlet weak var mealName: UILabel!
    @IBOutlet weak var mealTextField: UITextField!
    @IBOutlet weak var mealImage: UIImageView!
    
    var imagePickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mealName.text = meal?.name
        mealImage.image = meal?.image
        
    }

    @IBAction func changeMealName(_ sender: Any) {
        mealName.text = mealTextField.text
    }
    
    @IBAction func imageTapped(_ sender: UITapGestureRecognizer) {
        
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        
        present(imagePickerController,animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("Image error")
        }
        
        mealImage.image = image
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        mealName.text = mealTextField.text
    }
    
}

